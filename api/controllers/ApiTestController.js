/**
 * ApiTestController
 *
 * @description :: Server-side logic for managing apitests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getApi: function (req, res, next) {
		var data = {
			"message":"Api Successfully Received"
		};
		res.json(data);
	}
};
