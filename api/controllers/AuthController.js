/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');

module.exports = {
	_config: {
		actions: false,
		shortcuts: false,
		rest: false
	},
	login: function (req, res, next) {
		passport.authenticate('local', function (err, user, info) {
			if (err) {
				return res.serverError(err);
			} else if (!user) {
				res.redirect('/login');
			}
			req.logIn(user, function (err) {
				if (err) {
					return res.serverError(err);
				} else {
					req.session.isAuthenticated = true;
					req.session.save();
					res.redirect('/protected');
				}
			});
		})(req, res, next);
	},
	logout: function (req, res, next) {
		req.logout();
		req.session.isAuthenticated = false;
		req.session.save();
		res.redirect('/login');
	},
	signup: function (req, res, next) {
		var newUser = {};
		var newPassport = {
			user: null, // To be set later
			email: req.param("email"),
			password: req.param("password"),
			loginType: "passport-local"
		};
		User.create(newUser).exec(function (err, user) {
			newPassport.user = user.id;
			Passport.create(newPassport).exec(function (err, passport) {
				if (err) {
					return res.serverError(err);
				} else {
					User.update({
						id: user.id
					}, {
						passport: passport.id
					}).exec(function (err, updatedUser) {
						if (err) {
							return res.serverError(err);
						} else {
							res.redirect('/login');
						}
					});
				}
			});
		});
	},

	google: function (req, res, next) {
		passport.authenticate('google', { scope: ['profile', 'email'] })(req, res, next);
	},
	googleCallback: function (req, res, next) {
		req.session.isAuthenticated = true;
		req.session.save();
		res.redirect('/protected');
	},

	facebook: function (req, res, next) {
		passport.authenticate('facebook', { scope : ['email','public_profile'] })(req, res, next);
	},
	facebookCallback: function (req, res, next) {
		req.session.isAuthenticated = true;
		req.session.save();
		res.redirect('/protected');
	},

	twitter: function (req, res, next) {
		passport.authenticate('twitter', { scope: 'include_email' })(req, res, next);
	},
	twitterCallback: function (req, res, next) {
		req.session.isAuthenticated = true;
		req.session.save();
		res.redirect('/protected');
	},

	linkedin: function (req, res, next) {
		passport.authenticate('linkedin', { scope: ['r_basicprofile', 'r_emailaddress'] })(req, res, next);
	},
	linkedinCallback: function (req, res, next) {
		req.session.isAuthenticated = true;
		req.session.save();
		res.redirect('/protected');
	}
};
