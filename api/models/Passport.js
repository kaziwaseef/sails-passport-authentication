/**
 * Passport.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');

var supportedLogins = [
  'passport-local', // username && password
  'passport-google-oauth20', // google+ || gmail
  'passport-facebook', // facebook
  'passport-twitter', // twitter
  'passport-linkedin', // linkedin
  'passport-localapikey' // API authentication for external clients
];

module.exports = {

	attributes: {
		user: {
			model: 'user',
			via: 'passport'
		},
		loginType: {
			type: 'string',
			enum: supportedLogins
		},
		email: {
			type: 'email',
      unique: true
		},
		// For passport-local
		password: {
			type: 'string'
		},
		// For passport-google-oauth20 passport-facebook
		oAuthUniqueID: {
			type: 'string',
      unique: true
		},
		toJSON: function () {
			var obj = this.toObject();
			delete obj.password;
			delete obj.oAuthUniqueID;
			return obj;
		}
	},
	beforeCreate: function (passport, cb) {
    if(passport.password == undefined) {
      console.log('No Password Provided');
      cb();
    } else {
      bcrypt.genSalt(10, function (err, salt) {
  			bcrypt.hash(passport.password, salt, function (err, hash) {
  				if (err) {
  					console.log(err);
  					cb(err);
  				} else {
  					passport.password = hash;
  					cb();
  				}
  			});
  		});
    }
	}
};
