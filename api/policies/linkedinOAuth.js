/**
 * googleOauth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */

var passport = require('passport');

module.exports = function (req, res, next) {
	passport.authenticate('linkedin', { failureRedirect: '/login' })(req, res, next);
};
