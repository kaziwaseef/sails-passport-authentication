# Sails Social Login with Passport.js

How to use:

- Install bcrypt in a separate project to make sure it installs correctly. To install it you need to have node-gyp and python installed. In linux you need g++ compiler. In Windows you need Visual Studio with C#. After that run in a separate project:

~~~~
$ npm install bcrypt --save
~~~~

- If it works then clone this repository. Go to the directory and run:

~~~~
$ npm install
~~~~

- Create an app for Google, Facebook, Twitter and Linkedin (Google how to do that). You will receive an Id and a Secret.
- Create a file 'config/local.js'
- In this file write the personal information required. This file is ignored by git so that your sensitive information is not passed to the repository.

~~~~
module.exports = {
  connections: {
    localMysqlServer: {
      adapter: 'sails-mysql',
      host: 'Your Database Host',
      user: 'Your Database Username',
      password: 'Your Database Password',
      database: 'Your Database Schema Name'
    }
  },
  models: {
    connection: 'localMysqlServer',
    migrate: 'alter'
  },

  googleOAuthClientID: 'Your Google App ID',
  googleOAuthClientSecret: 'Your Google App Secret',

  facebookOAuthClientID: 'Your Facebook App ID',
  facebookOAuthClientSecret: 'Your Facebook App Secret',

  twitterOAuthClientID: 'Your Twitter App ID',
  twitterOAuthClientSecret: 'Your Twitter App Secret',

  linkedinOAuthClientID: 'Your Linkedin App ID',
  linkedinOAuthClientSecret: 'Your Linkedin App Secret'
}
~~~~
