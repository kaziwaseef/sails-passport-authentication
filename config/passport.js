var LocalConfig = require('./local');

var bcrypt = require('bcrypt');

var passport = require('passport');

var LocalStrategy = require('passport-local').Strategy;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var LinkedInStrategy = require('passport-linkedin').Strategy;
// var LocalApiKeyStrategy = require('passport-localapikey').Strategy;

passport.serializeUser(function (user, done) {
	// console.log('*****************\nIn serializeUser\n*****************');
	// console.log(user);
	if (typeof(user.id) != 'undefined') {
		done(null, user.id);
	} else if (typeof(user[0]) != 'undefined') {
		done(null, user[0].id);
	}
});

passport.deserializeUser(function (id, done) {
	// console.log('*****************\nIn deserializeUser\n*****************');
	User.findOne({
		id: id
	}, function (err, user) {
		done(err, user);
	});
});

passport.use(new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password'
	},
	function (email, password, done) {

		Passport.findOne({
			email: email
		}).exec(function (err, passport) {
			if (err) {
				return res.serverError(err);
			}
			if (!passport) {
				return done(null, false, {
					message: 'Incorrect email.'
				});
			}

			bcrypt.compare(password, passport.password, function (err, res) {
				if (!res)
					return done(null, false, {
						message: 'Invalid Password'
					});
				var returnUser = {
					email: passport.email,
					createdAt: passport.createdAt,
					id: passport.user
				};
				return done(null, returnUser, {
					message: 'Logged In Successfully'
				});
			});
		});
	}
));

passport.use(new GoogleStrategy({
		clientID: LocalConfig.googleOAuthClientID,
		clientSecret: LocalConfig.googleOAuthClientSecret,
		callbackURL: 'http://localhost:1337/auth/google/callback'
	},
	function (accessToken, refreshToken, profile, cb) {
		// console.log('*****************\nIn Strategy\n*****************');
		// console.log('ID: ' + profile.id);
		// console.log('Name: ' + profile.name.givenName + ' ' + profile.name.familyName);
		// console.log('Email: ' + profile.emails[0].value);
		// console.log('Gender: ' + profile.gender);
		// console.log('Photo: ' + profile.photos[0].value);
		var loginParameters = {
			loginType: 'passport-google-oauth20',
			email: profile.emails[0].value,
			oAuthUniqueID: profile.id
		};
		// console.log('*****************\nIn Find or Create\n*****************');
		Passport.find(loginParameters).exec(function (err, passport) {
			if (passport.length == 0) {
				// console.log('*****************\nIn Create\n*****************');
				Passport.create(loginParameters).exec(function (err, passportcreated) {
					console.log(passportcreated);
					User.create({
						passport: passportcreated.id
					}).exec(function (err, user) {
						Passport.update(loginParameters, {
							user: user.id
						}).exec(function (err, updatePassport) {
							return cb(err, user);
						});
					});
				});
			} else {
				// console.log('*****************\nIn Find\n*****************');
				User.find({
					passport: passport.id
				}).exec(function (err, user) {
					return cb(err, passport);
				});
			}
		});
	}
));

passport.use(new FacebookStrategy({
		clientID: LocalConfig.facebookOAuthClientID,
		clientSecret: LocalConfig.facebookOAuthClientSecret,
		callbackURL: "http://localhost:1337/auth/facebook/callback",
		profileFields: ['id', 'name', 'gender', 'photos', 'email'],
		enableProof: true
	},
	function (accessToken, refreshToken, profile, cb) {
		// console.log('*****************\nIn Strategy\n*****************');
		// console.log('ID: ' + profile.id);
		// console.log('Name: ' + profile.name.givenName + ' ' + profile.name.middleName + ' ' + profile.name.familyName);
		// console.log('Email: ' + profile.emails[0].value);
		// console.log('Gender: ' + profile.gender);
		// console.log('Photo: ' + profile.photos[0].value);
		var loginParameters = {
			loginType: 'passport-facebook',
			email: profile.emails[0].value,
			oAuthUniqueID: profile.id
		};
		// console.log('*****************\nIn Find or Create\n*****************');
		Passport.find(loginParameters).exec(function (err, passport) {
			if (passport.length == 0) {
				// console.log('*****************\nIn Create\n*****************');
				Passport.create(loginParameters).exec(function (err, passportcreated) {
					console.log(passportcreated);
					User.create({
						passport: passportcreated.id
					}).exec(function (err, user) {
						Passport.update(loginParameters, {
							user: user.id
						}).exec(function (err, updatePassport) {
							return cb(err, user);
						});
					});
				});
			} else {
				// console.log('*****************\nIn Find\n*****************');
				User.find({
					passport: passport.id
				}).exec(function (err, user) {
					return cb(err, passport);
				});
			}
		});
	}
));

passport.use(new TwitterStrategy({
		consumerKey: LocalConfig.twitterOAuthClientID,
		consumerSecret: LocalConfig.twitterOAuthClientSecret,
		callbackURL: "http://localhost:1337/auth/twitter/callback"
	},
	function (token, tokenSecret, profile, cb) {
		// console.log('*****************\nIn Strategy\n*****************');
		// console.log('ID: ' + profile.id);
		// console.log('Name: ' + profile.displayName);
		// // console.log('Email: ' + profile.emails[0].value);
		// // console.log('Gender: ' + profile.gender);
		// console.log('Photo: ' + profile.photos[0].value);
		var loginParameters = {
			loginType: 'passport-twitter',
			// email: profile.emails[0].value,
			email: null,
			oAuthUniqueID: profile.id
		};
		// console.log('*****************\nIn Find or Create\n*****************');
		Passport.find(loginParameters).exec(function (err, passport) {
			if (passport.length == 0) {
				// console.log('*****************\nIn Create\n*****************');
				Passport.create(loginParameters).exec(function (err, passportcreated) {
					console.log(passportcreated);
					User.create({
						passport: passportcreated.id
					}).exec(function (err, user) {
						Passport.update(loginParameters, {
							user: user.id
						}).exec(function (err, updatePassport) {
							return cb(err, user);
						});
					});
				});
			} else {
				// console.log('*****************\nIn Find\n*****************');
				User.find({
					passport: passport.id
				}).exec(function (err, user) {
					return cb(err, passport);
				});
			}
		});
	}
));

passport.use(new LinkedInStrategy({
		consumerKey: LocalConfig.linkedinOAuthClientID,
		consumerSecret: LocalConfig.linkedinOAuthClientSecret,
		callbackURL: "http://localhost:1337/auth/linkedin/callback"
	},
	function (token, tokenSecret, profile, cb) {
		// console.log('*****************\nIn Strategy\n*****************');
		// console.log('ID: ' + profile.id);
		// console.log('Name: ' + profile.name.givenName + ' ' + profile.name.familyName);
		// // console.log('Email: ' + profile.emails[0].value);
		// // console.log('Gender: ' + profile.gender);
		// // console.log('Photo: ' + profile.photos[0].value);
		var loginParameters = {
			loginType: 'passport-linkedin',
			// email: profile.emails[0].value,
			email: null,
			oAuthUniqueID: profile.id
		};
		// console.log('*****************\nIn Find or Create\n*****************');
		Passport.find(loginParameters).exec(function (err, passport) {
			if (passport.length == 0) {
				// console.log('*****************\nIn Create\n*****************');
				Passport.create(loginParameters).exec(function (err, passportcreated) {
					console.log(passportcreated);
					User.create({
						passport: passportcreated.id
					}).exec(function (err, user) {
						Passport.update(loginParameters, {
							user: user.id
						}).exec(function (err, updatePassport) {
							return cb(err, user);
						});
					});
				});
			} else {
				// console.log('*****************\nIn Find\n*****************');
				User.find({
					passport: passport.id
				}).exec(function (err, user) {
					return cb(err, passport);
				});
			}
		});
	}
));
